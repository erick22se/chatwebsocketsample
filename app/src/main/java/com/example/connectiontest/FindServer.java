package com.example.connectiontest;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class FindServer extends AppCompatActivity {

    EditText editIp;
    Button btnFindServer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_server);

        editIp = findViewById(R.id.editIpServer);
        btnFindServer = findViewById(R.id.btnFindServer);

        btnFindServer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String ipServer =  editIp.getText().toString();
                if(!ipServer.isEmpty()){
                    Intent intent = new Intent(FindServer.this,ClientActivity.class);
                    intent.putExtra("ipServer",ipServer);
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(),"Ingrese una Ip",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
